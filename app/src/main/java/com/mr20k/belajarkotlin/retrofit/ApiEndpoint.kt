package com.mr20k.belajarkotlin.retrofit

import com.mr20k.belajarkotlin.MainModel
import retrofit2.Call
import retrofit2.http.GET

interface ApiEndpoint {
    @GET("data.php")
    fun getData() : Call<MainModel>
}