package com.mr20k.belajarkotlin

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.adapter_main.view.*

class MainAdapter (var results: ArrayList<MainModel.Result>, private val listener: OnAdapterListener):
    RecyclerView.Adapter<MainAdapter.ViewHolder>() {

    private val TAG : String = "MainAdapter"

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder (
        LayoutInflater.from( parent.context ).inflate( R.layout.adapter_main,
            parent, false)
    )

    override fun getItemCount() = results.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val result = results[position]

        with(holder) {
            view.textView.text = result.title
            printLog("result.image : ${result.image}")

            Glide.with(view)
                .load( result.image )
                .placeholder( R.drawable.img_placeholder )
                .error( R.drawable.img_placeholder )
                .centerCrop()
                .into( view.imageView )

            view.setOnClickListener{
                listener.onClick(result)
            }
        }
    }

    class ViewHolder(val view: View): RecyclerView.ViewHolder(view)

    fun setData(data: List<MainModel.Result>){
        this.results.clear()
        this.results.addAll(data)
        notifyDataSetChanged()
    }

    interface OnAdapterListener {
        fun onClick(result: MainModel.Result)
    }

    private fun printLog(msg: String){
        Log.d(TAG, msg)
    }
}
