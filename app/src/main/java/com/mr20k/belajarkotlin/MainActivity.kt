package com.mr20k.belajarkotlin

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.mr20k.belajarkotlin.retrofit.ApiService
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

	private lateinit var mainAdapter: MainAdapter

	private val TAG : String = "MainActivity"
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)
	}

	override fun onStart() {
		super.onStart()
		setupRecyclerView()
		getDataFromApi()
	}

	private fun setupRecyclerView(){
		mainAdapter = MainAdapter(arrayListOf(), object : MainAdapter.OnAdapterListener {
			override fun onClick(result: MainModel.Result) {
				printLog("${result}")
//				Toast.makeText(applicationContext, result.title, Toast.LENGTH_SHORT).show()
				startActivity(
					Intent(applicationContext, DetailActivity::class.java)
						.putExtra("intent_title", result.title)
						.putExtra("intent_image", result.image)
				)
			}
		})
		
		recyclerView.apply {
			layoutManager = LinearLayoutManager(applicationContext)
			adapter = mainAdapter
		}

	}

	private fun getDataFromApi(){
		progressBar.visibility = View.VISIBLE
		ApiService.endpoint.getData()
			.enqueue(object : Callback<MainModel> {
				override fun onResponse(
					call: Call<MainModel>,
					response: Response<MainModel>
				) {
					progressBar.visibility = View.GONE
					if (response.isSuccessful){
						showData(response.body()!!)
					}
				}

				override fun onFailure(call: Call<MainModel>, t: Throwable) {
					progressBar.visibility = View.GONE
					printLog(t.toString())
				}

			})
	}

	private  fun printLog(msg: String){
		Log.d(TAG, msg)
	}

	private fun showData(data : MainModel){
		val results = data.result

		mainAdapter.setData(results)

		for (result in results){
			printLog("Title : ${result.title}")
		}

	}
}